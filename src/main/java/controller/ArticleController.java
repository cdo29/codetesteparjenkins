package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import model.exposed.Article;
import model.exposed.Message;
import model.persistence.ArticleJPA;
import repository.ArticleRepository;


@RestController
public class ArticleController {

	@Autowired
	ArticleRepository articleRepo;

	//static List<Article> articles = new ArrayList<Article>();

	@PostMapping(value = "/article")
	public ResponseEntity<Message> creerArticle(
			@ModelAttribute("article") model.persistence.ArticleJPA articleJPA) {
		//articles.add(article);
		Message message = new Message();
		message.setMessage("L' article " +  " a bien été créé");
		articleRepo.save(articleJPA);
		return new ResponseEntity<>(message, HttpStatus.CREATED);
	}

	@GetMapping(value = "/article/{id}")
	public ResponseEntity<Article> afficherUnArticle(@PathVariable Long id) {
//		for (Article article : articles) {
//			if (article.getId() == id ) {
//				return new ResponseEntity<>(article, HttpStatus.OK);
//			}
//		}
		
		ArticleJPA articleJPA = articleRepo.findById(id).get();
		Article article = new Article(articleJPA);
		if(articleRepo.findById(id).isPresent()) {
			return new ResponseEntity<Article>(article, HttpStatus.OK);
		} else {
			return new ResponseEntity<Article>(HttpStatus.NOT_FOUND);
		}
	}

//	@GetMapping(value = "/articles")
//	public ResponseEntity<List<Article>> afficherTousLesArticles() {
//		List<Article> articles = new ArrayList<Article>(articleRepo.findAll());
//		return new ResponseEntity<>(articles, HttpStatus.OK);
//	}

	@DeleteMapping(value = "/article/{id}")
	public ResponseEntity<Message> supprimerUnArticle(@PathVariable Long id) {
//		for (Article article : articles) {
//			if (article.getId() == id ) {
//				articles.remove(article);
//				return new ResponseEntity<>(article, HttpStatus.ACCEPTED);
//			}
//		}
		Message message = new Message();
		if(articleRepo.findById(id).isPresent()) {
			articleRepo.deleteById(id);
			message.setMessage("L'article a été supprimé");
			return new ResponseEntity<>(message, HttpStatus.OK);
		} else {
			message.setError("Il y a une erreur");
			return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
		}
	}
}