package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import model.Comment;
import model.exposed.Article;
import repository.CommentRepository;

@RestController
public class CommentController {
	
//	@Autowired
//	CommentRepository commentRepo;
//
//	@PostMapping(value = "/article/{id}/comment")
//	public ResponseEntity<Comment> creerCommentaire(	
//			@PathVariable("id") Long id,
//			@ModelAttribute("comment") Comment comment) {
//
//		for (Article article : ArticleController.articles) {
//			if(article.getId() == id) {
//
//				if(comment.getNote()<1 || comment.getNote()>5 ) {
//					return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//				}
//
//				comment.setDate(new Date());
//				article.getComments().add(comment);
//				return new ResponseEntity<>(comment, HttpStatus.CREATED);
//			}
//		}
//		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//
//	}
//
//	@GetMapping(value = "/article/{id}/comments")
//	public ResponseEntity<List<Comment>> afficherTousLesCommentaires(
//			@PathVariable("id") String id,
//			@RequestParam(value = "text", required = false)  Boolean isText) 
//	{	
//
//		List<Comment> CommentsWithText = new ArrayList<>();
//		List<Comment> CommentsWithoutText = new ArrayList<>();
//
//
//		for (Article article : ArticleController.articles) {
//
//			if(article.getId().equals(id)) {
//				if(isText == null) {
//					return new ResponseEntity<>(article.getComments(), HttpStatus.OK);
//				} else if(isText) {
//					for (Comment comment : article.getComments()) {
//						if(!(comment.getTexte() == null)) {
//							CommentsWithText.add(comment);
//						}
//					}
//
//					return new ResponseEntity<>(CommentsWithText, HttpStatus.OK);
//				}else if(!isText){
//
//					for (Comment comment : article.getComments()) {
//						if(comment.getTexte() == null) {
//							CommentsWithoutText.add(comment);
//						}
//					}
//					return new ResponseEntity<>(CommentsWithoutText, HttpStatus.OK);
//				}
//			}
//		}
//		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//	}
//
//
//	@GetMapping(value = "/comment/{id}")
//	public ResponseEntity<Comment> afficherUnCommentaire(@PathVariable Long id) {
//		for (Article article : ArticleController.articles) {
//			for (Comment comment : article.getComments()) {
//				if(comment.getId()== id) {
//
//					return new ResponseEntity<>(comment, HttpStatus.OK);
//				}		 
//			}
//		}
//		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//	}
//
//
//	@DeleteMapping(value = "/comment/{id}")
//	public ResponseEntity<Comment> supprimerUnCommentaire(@PathVariable String id) {
//		for (Article article : ArticleController.articles) {
//			for (Comment comment : article.getComments()) {
//				article.getComments().remove(comment);
//				return new ResponseEntity<>(comment, HttpStatus.OK); 
//			}
//		}
//		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//	}

}