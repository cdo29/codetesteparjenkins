package model.exposed;

import model.persistence.ArticleJPA;

public class Article {
	protected Long id;
	protected String nom;
	protected float prix;

	public Article(ArticleJPA articleJPA) {
		this.id = articleJPA.getId();
		this.nom = articleJPA.getNom();
		this.prix = articleJPA.getPrix();
	}
	
	


	public Article() {

	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public float getPrix() {
		return prix;
	}


	public void setPrix(float prix) {
		this.prix = prix;
	}

}
