package model.persistence;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ArticleJPA{

	@Id	@GeneratedValue
	private Long id;
	
	protected String nom;
	protected float prix;
	
	@OneToMany(mappedBy="articleJPA")
	private List<CommentJPA> comments;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	public List<CommentJPA> getComments() {
		return comments;
	}

	public void setComments(List<CommentJPA> comments) {
		this.comments = comments;
	}

	
}
