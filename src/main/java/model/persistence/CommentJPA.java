package model.persistence;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class CommentJPA{

	@Id	@GeneratedValue
	private Long id;
	
	private int Note;
	
	private Date date;
	private String texte;
	
	@ManyToOne
	private ArticleJPA articleJPA;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNote() {
		return Note;
	}

	public void setNote(int note) {
		Note = note;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTexte() {
		return texte;
	}

	public void setTexte(String texte) {
		this.texte = texte;
	}

	public ArticleJPA getArticleJPA() {
		return articleJPA;
	}

	public void setArticleJPA(ArticleJPA articleJPA) {
		this.articleJPA = articleJPA;
	}


	
	
}
