package repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.persistence.CommentJPA;

@Repository
public interface CommentRepository extends JpaRepository<CommentJPA, Long> {
	
}